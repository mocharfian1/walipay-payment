// import axios from "axios";
import ApiService from "@/services/ApiService";
import Vue from "vue";
import VueRouter from 'vue-router';

Vue.use(VueRouter);

function isValidJSON(str){
    try {
        JSON.parse(str);
        return true;
    } catch (error) {
        return false;
    }
}

export default {
    // eslint-disable-next-line no-unused-vars
    async findMenuByPath({ commit }, { obj, path, isTranslated = false }) {
        for (const key in obj) {
            if (typeof obj[key] === 'string') {
                const matches = obj[key] === path;
                if (matches) {
                    if(isTranslated){
                        this.state.menu.menuSelectedTranslated = obj
                    } else {
                        commit("SET_MENU_SELECTED", obj)
                    }
                }
            } else if (typeof obj[key] === 'object') {
                this.dispatch("findMenuByPath", { obj: obj[key], path, isTranslated });
            }
        }
    },
    findChineseWords({ commit }, obj) {
        for (const key in obj) {
            if (isValidJSON(obj[key])) {
                this.dispatch("findChineseWords", JSON.parse(obj[key]));
            } else if (typeof obj[key] === 'string'){
                const matches = obj[key].match(/[\u4e00-\u9fa5]/g);
                if (matches) {
                    let ok = obj[key]
                    commit("SET_CHINESE_WORDS", ok)
                }
            } else if (typeof obj[key] === 'object') {
                this.dispatch("findChineseWords", obj[key]);
            }
        }
    },
    async translateChineseToEnglish({ commit }, text){
        let toLang = "en-US"
        try {
            let filterText = String(text).substr(0, text.length - 1)

            // if (filterText.endsWith('%')) {
            //     filterText = filterText.slice(0, -1);
            // }

            const translate = await ApiService.getTranslate(`https://translate.googleapis.com/translate_a/single?client=gtx&ie=UTF-8&oe=UTF-8&dt=t&sl=zh-CN&tl=${toLang}&q=${encodeURIComponent(filterText)}`);

            let data = translate.data[0];

            data?.forEach(r => {
                commit("SET_ENGLISH_WORDS", r[0])
            })
        } catch (e){
            console.log("==> ERR", e)
        }
    },
    async translateChineseToExpectedLang({ commit }, text){
        let toLang = localStorage.getItem('lang')
        try {
            let filterText = String(text).substr(0, text.length - 1)

            // if (filterText.endsWith('%')) {
            //     filterText = filterText.slice(0, -1);
            // }

            const translate = await ApiService.getTranslate(`https://translate.googleapis.com/translate_a/single?client=gtx&ie=UTF-8&oe=UTF-8&dt=t&sl=en-US&tl=${toLang}&q=${encodeURIComponent(filterText)}`);

            let data = translate.data[0];

            data?.forEach(r => {
                commit("SET_EXPECTED_WORDS", r[0])
            })
        } catch (e){
            console.log("==> ERR", e)
        }
    },
    async replaceResponseToEnglish({ commit }, strResponse){

        let responseText = JSON.stringify(strResponse)
        let wordChinese = String(this.state.endpoint.wordChinese).split("|")
        let wordEnglish = String(this.state.endpoint.wordExpected).split("|")

        await wordChinese.forEach((value, index)=>{
            if(String(value).trim().length > 0){
                responseText = responseText.replace(value, wordEnglish[index])
            }
        })


        commit('SET_RESPONSE_DATA_ENGLISH', responseText);
        return responseText;
    },
    async hitEndpoint({ commit }, { path, method= null, data = null, patchToVar = null }) {
        this.state.endpoint.responseData = ""
        this.state.endpoint.responseDataEnglish = ""
        this.state.endpoint.wordChinese = ""
        this.state.endpoint.wordEnglish = ""
        this.state.endpoint.wordExpected = ""

        let responseTranslated = ""
        try {
            const getMenu = await ApiService.post("/auth", {
                path: `/customize-oa/${path}`,
                method,
                data
            })



            if(getMenu.status === 200 && getMenu.data.code === 200){
                await this.dispatch("findChineseWords", getMenu.data);
                if(path.includes("rich")){
                    console.log("===> Chinese", this.state.endpoint.wordChinese)
                }

                await this.dispatch("translateChineseToEnglish", this.state.endpoint.wordChinese)

                if(localStorage.getItem("lang") !== "en-US"){
                    await this.dispatch("translateChineseToExpectedLang", this.state.endpoint.wordEnglish)
                } else {
                    this.state.endpoint.wordExpected = this.state.endpoint.wordEnglish
                }


                responseTranslated = await this.dispatch("replaceResponseToEnglish", getMenu.data)
            }


            commit('SET_RESPONSE_DATA', getMenu.data);

            if(patchToVar != null){
                commit('SET_MAPPING_TO_VAR', { key: patchToVar, original: getMenu.data, translated: responseTranslated });
            }
            return { original: getMenu.data, translated: responseTranslated };
        } catch (error) {
            console.error('Error hitting endpoint:', error);
            // Tampilkan pesan kesalahan kepada pengguna jika diperlukan
        }
    },
    async login({ commit }, { username, password }){
        const getLogin = await ApiService.post("/auth",
            {
                "path": `/customize-oa/auth/login?username=${username}&password=${password}`
            }
        )

        if(getLogin.status === 200 && getLogin.data.code === 200){
            commit('SET_SESSION', getLogin.headers['x-token'])
            return { login: true };
        } else {
            return { login: false };
        }
    },
    async translateConstants({ commit }, {keys, values}){
        let toLang = localStorage.getItem('lang')
        try {
            let filterText = values

            const translate = await ApiService.getTranslate(`https://translate.googleapis.com/translate_a/single?client=gtx&ie=UTF-8&oe=UTF-8&dt=t&sl=zh-CN&tl=${toLang}&q=${encodeURIComponent(filterText)}`);

            let data = translate.data[0];

            data?.forEach(r => {
                commit("SET_CONSTANTS_TRANSLATED_WORDS", { keys, values: r[0]})
            })
        } catch (e){
            console.log("==> ERR", e)
        }
    },
}