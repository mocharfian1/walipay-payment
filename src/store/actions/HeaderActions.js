// import axios from "axios";
// import ApiService from "@/services/ApiService";

export default {
    // eslint-disable-next-line no-unused-vars
    async headerMapping({ commit }, { data }){
        this.state.endpoint.responseData = ""
        this.state.endpoint.responseDataEnglish = ""
        this.state.endpoint.wordChinese = ""
        this.state.endpoint.wordEnglish = ""
        this.state.endpoint.wordExpected = ""

        await this.dispatch("findChineseWords", data)
        await this.dispatch("translateChineseToEnglish", this.state.endpoint.wordChinese)
        return await this.dispatch("replaceResponseToEnglish", data)
    }
}