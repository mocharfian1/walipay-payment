export default {
    endpoint: {
        selectedMenuId: null,
        responseData: null,
        responseDataEnglish: null,
        session: null,
        wordChinese: "",
        wordEnglish: "",
        wordExpected: "",
        constantsTranslated: ""
    }
}