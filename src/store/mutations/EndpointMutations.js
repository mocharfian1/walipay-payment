import Vue from "vue";

export default {
    SET_RESPONSE_DATA(state, data) {
        state.endpoint.responseData = data;
    },
    SET_RESPONSE_DATA_ENGLISH(state, data) {
        state.endpoint.responseDataEnglish = data;
    },
    SET_SESSION(state, sessionToken){
        state.endpoint.session = String(sessionToken).split("SESSION=")[1];
        Vue.$cookies.set("SESSION", state.endpoint.session)
    },
    SET_CHINESE_WORDS(state, data){
        state.endpoint.wordChinese += String(data).trim() + "|"
    },
    SET_ENGLISH_WORDS(state, data){
        state.endpoint.wordEnglish += String(data).trim()
    },
    SET_EXPECTED_WORDS(state, data){
        state.endpoint.wordExpected += String(data).trim()
    },
    SET_CONSTANTS_TRANSLATED_WORDS(state, { keys, values }){
        let arrValues = String(values).split("|")
        let combined = {}
        keys.forEach((key, index) => {
            combined[key] = arrValues[index];
        });

        state.constants = combined
    },
    SET_MAPPING_TO_VAR(state, { key, original, translated }) {
        state.endpoint[key] = { original, translated }
    }
}