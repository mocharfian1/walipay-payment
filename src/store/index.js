// index.js

import Vue from 'vue';
import Vuex from 'vuex';
// import ApiService from '@/services/ApiService';
import VueCookie from "vue-cookies";
// import axios from "axios";

import EndpointActions from './actions/EndpointActions'
import EndpointMutations from "@/store/mutations/EndpointMutations";
import EndpointStates from "@/store/states/EndpointStates";
import MenuStates from "@/store/states/MenuStates";
import HeaderActions from "@/store/actions/HeaderActions";
import MenuMutations from "@/store/mutations/MenuMutations";
import ConstantStates from "@/store/states/ConstantStates";

Vue.use(Vuex);
Vue.use(VueCookie);

export default new Vuex.Store({
    state: {
        ...EndpointStates,
        ...MenuStates,
        ...ConstantStates
    },
    mutations: {
        ...EndpointMutations,
        ...MenuMutations
    },
    actions: {
        ...EndpointActions,
        ...HeaderActions
    },
});