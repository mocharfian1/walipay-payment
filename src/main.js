import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import router from './router';
import store from './store';
import VueCookie from 'vue-cookies';
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import '@mdi/font/css/materialdesignicons.css'
import Constants from "@/helpers/Constants";


Vue.config.productionTip = false

// Set Language
const lang = localStorage.getItem('lang') ?? 'en-US'
localStorage.setItem('lang', lang)

new Vue({
  router,
  store,
  vuetify,
  VueCookie,
  render: h => h(App)
}).$mount('#app')

let constants = Object.values(Constants)
store.dispatch("translateConstants", {keys: Object.keys(Constants), values:constants.join("|")})
