export default {
    ADD_FILTER: 'Tambahkan Filter',
    CONDITION: 'Kondisi',
    FIELD: 'Kolom',
    VALUE: 'Nilai',
    VALUE_2: 'Nilai 2',
    ADD: 'Tambah',
    LOADING_1: 'Sedang memuat data. Harap menunggu',
    ADMIN_PANEL: 'Panel Admin',
    TOTAL_STATISTIC_RECEIPT: 'STATISTIK TOTAL PENERIMAAN'

}