// ApiService.js

import axios from 'axios';
import Vue from "vue";
import VueCookie from "vue-cookies";

Vue.use(VueCookie)

// const ApiService = axios.create({
//   baseURL: 'http://walipay-paymen.mobileapi.masuk.id', // Ganti dengan URL backend Anda
//   headers: {
//     'Content-Type': 'application/json',
//     "x-token": Vue.$cookies.get("SESSION")
//   },
// });
//
// export default ApiService;

class AxiosQueue {
  constructor() {
    this.queue = [];
    this.isProcessing = false;
  }

  addRequest(config) {
    return new Promise((resolve, reject) => {
      this.queue.push({ config, resolve, reject });
      if (!this.isProcessing) {
        this.processQueue();
      }
    });
  }

  async processQueue() {
    this.isProcessing = true;
    while (this.queue.length > 0) {
      const { config, resolve, reject } = this.queue.shift();
      try {
        const response = await axios(config);
        resolve(response);
      } catch (error) {
        reject(error);
      }
    }
    this.isProcessing = false;
  }
}

const axiosQueue = new AxiosQueue();

function fetchData(method, url, data) {
  return axiosQueue.addRequest({ method: method, url: "//walipay-paymen.mobileapi.masuk.id" + url, data,
      headers: {
        'Content-Type': 'application/json',
        "x-token": Vue.$cookies.get("SESSION")
      },
  });
}

function fetchDataTranslate(url) {
  return axiosQueue.addRequest({ method: 'get', url});
}

const ApiService = {
  post: function (url, data){
    return fetchData('post', url, data)
  },
  get: function (url, data){
    return fetchData('get', url, data)
  },
  getTranslate: function (url){
    return fetchDataTranslate(url);
  }
}

export default ApiService;