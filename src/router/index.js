// index.js

import Vue from 'vue';
import VueRouter from 'vue-router';
import LoginPage from '../pages/LoginPage.vue';
import DashboardView from "@/views/DashboardView";

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'DashboardView',
    component: DashboardView,
  },
  {
    path: '/page/home',
    name: 'DashboardView',
    component: DashboardView,
  },
  {
    path: '/login',
    name: 'LoginPage',
    component: LoginPage,
  }
  // Tambahkan rute lainnya di sini
];

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes,
});

export default router;
